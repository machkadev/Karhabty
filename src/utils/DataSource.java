/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 3D-Artist
 */
public class DataSource {
    
    private String url;
    private String login;
    private String password;
    private Connection connection;
    private static DataSource instance;

    private DataSource() {
        
        try {
            url = "jdbc:mysql://localhost:3306/karhabty";
            login = "root";
            password = "";
            connection = DriverManager.getConnection(url, login, password);
            System.out.println("Connection GOOD");
        } catch (SQLException ex) {
            Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    public Connection getConnection(){
        return connection;
    }

    public static DataSource getInstance() {
        if (instance == null) {
            instance = new DataSource();
        }
        return instance;
    }
    
}
