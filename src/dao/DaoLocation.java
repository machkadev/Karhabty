/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import entity.Agence;
import entity.Chauffeur;
import entity.Location;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DataSource;



public class DaoLocation {
    
    private final Connection cnx;
    private static DaoLocation instance;
    
    private DaoLocation() {
        cnx = DataSource.getInstance().getConnection();
    }
    
    public static DaoLocation getInstance()
    {
        if (instance == null) {
            instance = new DaoLocation();
        }
        return instance; 
    }
    
    
    public Location findLocationById(int idClient, int idAgence, String matricule)
    {
        Location location = new Location();
        int count = 0;
           
        String requete="select * from location where id_client="+idClient+" and id_agence="+idAgence+" and matricule='"+matricule+"'";
        try{
            Statement st = cnx.createStatement();
            ResultSet rsl = st.executeQuery(requete);
            while(rsl.next())
            {
                location.setClient(DaoUser.getInstance().findUserById(rsl.getInt(1)));
                location.setAgence(DaoAgence.getInstance().getAgenceById(rsl.getInt(2)));
                location.setVoiture(DaoVoiture.getInstance().findById(rsl.getString(3)));
                location.setDateDebut(rsl.getDate(4));
                location.setDateFin(rsl.getDate(5));
                location.setPrixLocation(rsl.getFloat(6));
                location.setAvecChauffeur(rsl.getBoolean(7));
                location.setApprouved(rsl.getBoolean(8));
                
                count++;
            }
           if(count == 0){
                return null;
           }else{
               return location;
           }  
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoAgence.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
   }
    
    public List<Location> getLocationByAgence( int idAgence)
    {
        List<Location> list = new ArrayList<>();
        int count = 0;
           
        String requete="select * from location where id_agence="+idAgence;
        try{
            Statement st = cnx.createStatement();
            ResultSet rsl = st.executeQuery(requete);
            while(rsl.next())
            {
                Location location = new Location();
                location.setClient(DaoUser.getInstance().findUserById(rsl.getInt(1)));
                location.setAgence(DaoAgence.getInstance().getAgenceById(rsl.getInt(2)));
                location.setVoiture(DaoVoiture.getInstance().findById(rsl.getString(3)));
                location.setDateDebut(rsl.getDate(4));
                location.setDateFin(rsl.getDate(5));
                location.setPrixLocation(rsl.getFloat(6));
                location.setAvecChauffeur(rsl.getBoolean(7));
                location.setApprouved(rsl.getBoolean(8));
                list.add(location);
                count++;
            }
           if(count == 0){
                return null;
           }else{
               return list;
           }  
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoAgence.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
   }
    
    public List<Location> getLocationNonApprouvedByAgence( int idAgence)
    {
        List<Location> list = new ArrayList<>();
        int count = 0;
           
        String requete="select * from location where id_agence="+idAgence+" and approuved=0";
        try{
            Statement st = cnx.createStatement();
            ResultSet rsl = st.executeQuery(requete);
            while(rsl.next())
            {
                Location location = new Location();
                location.setClient(DaoUser.getInstance().findUserById(rsl.getInt(1)));
                location.setAgence(DaoAgence.getInstance().getAgenceById(rsl.getInt(2)));
                location.setVoiture(DaoVoiture.getInstance().findById(rsl.getString(3)));
                location.setDateDebut(rsl.getDate(4));
                location.setDateFin(rsl.getDate(5));
                location.setPrixLocation(rsl.getFloat(6));
                location.setAvecChauffeur(rsl.getBoolean(7));
                location.setApprouved(rsl.getBoolean(8));
                list.add(location);
                count++;
            }
           if(count == 0){
                return null;
           }else{
               return list;
           }  
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoAgence.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
   }
    
    public boolean ajouterLocation(Location location)
    {
        int numberRows;
        try {
            String requete = "insert into location set id_client=?,id_agence=?,matricule=?, date_debut=?, date_fin=?, prix_location=?, chauffeur=?";
            PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setInt(1,location.getClient().getId());
            pst.setInt(2,location.getAgence().getId_agence());
            pst.setString(3,location.getVoiture().getMatricule());
            pst.setDate(4,location.getDateDebut());
            pst.setDate(5,location.getDateFin());
            pst.setFloat(6,location.getPrixLocation());
            pst.setBoolean(7,location.isAvecChauffeur());
            
            numberRows = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(numberRows == 0){
            return false;
        }else{
            return true;
        }
    }
    
    
    public boolean approuverLocation(Location location)
    {
        int numberRows;
        try {
            String requete = "update location set approuved=true where id_client=? and id_agence=? and matricule=?";
            PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setInt(1,location.getClient().getId());
            pst.setInt(2,location.getAgence().getId_agence());
            pst.setString(3,location.getVoiture().getMatricule());
            
            numberRows = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(numberRows == 0){
            return false;
        }else{
            return true;
        }
    }
    
    
    public boolean supprimerLocation(Location location)
    {
        int numberRows;
        try {
            String requete = "delete from location where id_client=? and id_agence=? and matricule=?";
            PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setInt(1,location.getClient().getId());
            pst.setInt(2,location.getAgence().getId_agence());
            pst.setString(3,location.getVoiture().getMatricule());
            numberRows = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(numberRows == 0){
            return false;
        }else{
            return true;
        }
    }
    
     public List<Location> getAllLocationByManager(int idManager){
          List<Location> listAllLocation = new ArrayList<>();
          List<Agence> listeAgenceByManager = new ArrayList<>();
          listeAgenceByManager = DaoAgence.getInstance().getAgencesByManagerID(idManager);
          if(listeAgenceByManager != null){
                for(int i=0; i<listeAgenceByManager.size();i++){
                    List<Location> listAllLocationByAgence = new ArrayList<>();
                    listAllLocationByAgence = this.getLocationByAgence(listeAgenceByManager.get(i).getId_agence());
                    if(listAllLocationByAgence != null){
                        listAllLocation.addAll(listAllLocationByAgence);
                    }
                }
          }
                  
          return listAllLocation;
          
     }
     
     public List<Location> getAllLocationNonApprouvedByManager(int idManager){
          List<Location> listAllLocation = new ArrayList<>();
          List<Agence> listeAgenceByManager = new ArrayList<>();
          listeAgenceByManager = DaoAgence.getInstance().getAgencesByManagerID(idManager);
          if(listeAgenceByManager != null){
                for(int i=0; i<listeAgenceByManager.size();i++){
                    List<Location> listAllLocationByAgence = new ArrayList<>();
                    listAllLocationByAgence = this.getLocationNonApprouvedByAgence(listeAgenceByManager.get(i).getId_agence());
                    if(listAllLocationByAgence != null){
                        listAllLocation.addAll(listAllLocationByAgence);
                    }
                }
          }
                  
          return listAllLocation;
          
     }
     
     
     public List<Location> getLocationByVoiture(String idVoiture)
    {
        List<Location> list = new ArrayList<>();
        int count = 0;
           
        String requete="select * from location where matricule='"+idVoiture+"'";
        try{
            Statement st = cnx.createStatement();
            ResultSet rsl = st.executeQuery(requete);
            while(rsl.next())
            {
                Location location = new Location();
                location.setClient(DaoUser.getInstance().findUserById(rsl.getInt(1)));
                location.setAgence(DaoAgence.getInstance().getAgenceById(rsl.getInt(2)));
                location.setVoiture(DaoVoiture.getInstance().findById(rsl.getString(3)));
                location.setDateDebut(rsl.getDate(4));
                location.setDateFin(rsl.getDate(5));
                location.setPrixLocation(rsl.getFloat(6));
                location.setAvecChauffeur(rsl.getBoolean(7));
                location.setApprouved(rsl.getBoolean(8));
                list.add(location);
                count++;
            }
           if(count == 0){
                return null;
           }else{
               return list;
           }  
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoAgence.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
   }
   

}  
     
     
     

