/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Agence;
import entity.Chauffeur;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DataSource;


public class DaoChauffeur {
    
    private Connection cnx;
    private static DaoChauffeur instance;
    
    private DaoChauffeur()
    {
        cnx = DataSource.getInstance().getConnection();
    }
    
    public static DaoChauffeur getInstance()
    {
        if (instance == null) {
            instance = new DaoChauffeur();
        }
        return instance;
    }
    
    public Chauffeur findById(int id)
    {
        Chauffeur chauffeur = new Chauffeur();
        int count = 0;
        try {
            String requete = "select * from chauffeur where id_chauffeur="+id;
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                count++;
                chauffeur.setId(rs.getInt(1));
                DaoVoiture daoV = DaoVoiture.getInstance();
                chauffeur.setVoiture(daoV.findById(rs.getString(2)));
                chauffeur.setNom(rs.getString(3));
                chauffeur.setAge(rs.getInt(4));
                DaoAgence daoAg = DaoAgence.getInstance();
                chauffeur.setAgence(daoAg.getAgenceById(rs.getInt(5)));
                chauffeur.setPrix(rs.getFloat(6));
                
            }
            if(count == 0){
                return null;
            }else{
                return chauffeur;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
    public boolean ajouterChauffeur(Chauffeur chauffeur)
    {
        int numberRows;
        try {
            String requete = "insert into chauffeur set id_voiture=?,nom_chauffeur=?,age_chauffeur=?, id_agence=?, prix_chauffeur=?";
            PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setString(1,chauffeur.getVoiture().getMatricule());
            pst.setString(2,chauffeur.getNom());
            pst.setInt(3,chauffeur.getAge());
            pst.setInt(4,chauffeur.getAgence().getId_agence());
            pst.setFloat(5,chauffeur.getPrix());
            numberRows = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(numberRows == 0){
            return false;
        }else{
            return true;
        }
    }
    
    public boolean supprimerChauffeur(int idChauffeur)
    {
        int numberRows;
        try {
            String requete = "delete from chauffeur where id_chauffeur=?";
            PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setInt(1,idChauffeur);
            numberRows = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(numberRows == 0){
            return false;
        }else{
            return true;
        }
    }
    
    public boolean modifierChauffeur(Chauffeur chauffeur)
    {
        int numberRows;
        try {
            String requete = "update chauffeur set id_voiture=?, nom_chauffeur=?, age_chauffeur=?,id_agence=?, prix_chauffeur=? where id_chauffeur=?";
            PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setString(1,chauffeur.getVoiture().getMatricule());
            pst.setString(2,chauffeur.getNom());
            pst.setInt(3,chauffeur.getAge());
            pst.setInt(4,chauffeur.getAgence().getId_agence());
            pst.setFloat(5,chauffeur.getPrix());
            pst.setInt(6,chauffeur.getId());
            numberRows = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(numberRows == 0){
            return false;
        }else{
            return true;
        }
    }
    
    public List<Chauffeur> getAllByAgence(int idAgence)
    {
        List<Chauffeur> listChauffeurs = new ArrayList<>();
        try {
            String requete = "select * from chauffeur where id_agence="+idAgence;
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                Chauffeur ch = new Chauffeur();
                ch.setId(rs.getInt(1));
                DaoVoiture daoV = DaoVoiture.getInstance();
                ch.setVoiture(daoV.findById(rs.getString(2)));
                ch.setNom(rs.getString(3));
                ch.setAge(rs.getInt(4));
                DaoAgence daoAg = DaoAgence.getInstance();
                ch.setAgence(daoAg.getAgenceById(rs.getInt(5)));
                ch.setPrix(rs.getFloat(6));
                listChauffeurs.add(ch);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listChauffeurs;
    }
    
    public List<Chauffeur> getAllByManager(int idManager)
    {
        List<Chauffeur> listChauffeurs = new ArrayList<>();
        
        List<Agence> listAgences = new ArrayList<>();
        listAgences = DaoAgence.getInstance().getAgencesByManagerID(idManager);
        for(int i =0; i<listAgences.size(); i++){
            List<Chauffeur> listChauffeursAgences = new ArrayList<>();
            listChauffeursAgences = this.getAllByAgence(listAgences.get(i).getId_agence());
            listChauffeurs.addAll(listChauffeursAgences);
        }
        return listChauffeurs;
    }
    
    
    public List<Chauffeur> getAllDisponibleByAgence(int idAgence)
    {
        List<Chauffeur> listChauffeurs = new ArrayList<>();
        try {
            String requete = "select * from chauffeur where id_agence="+idAgence+" AND id_voiture=null";
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                Chauffeur ch = new Chauffeur();
                ch.setId(rs.getInt(1));
                DaoVoiture daoV = DaoVoiture.getInstance();
                ch.setVoiture(daoV.findById(rs.getString(2)));
                ch.setNom(rs.getString(3));
                ch.setAge(rs.getInt(4));
                DaoAgence daoAg = DaoAgence.getInstance();
                ch.setAgence(daoAg.getAgenceById(rs.getInt(5)));
                ch.setPrix(rs.getFloat(6));
                listChauffeurs.add(ch);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listChauffeurs;
    }
    
    
    public Chauffeur findByNom(String nom, int idAgence)
    {
        Chauffeur chauffeur = new Chauffeur();
        int count = 0;
        try {
            String requete = "select * from chauffeur where nom_chauffeur='"+nom+"' and id_agence="+idAgence;
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                count++;
                chauffeur.setId(rs.getInt(1));
                DaoVoiture daoV = DaoVoiture.getInstance();
                chauffeur.setVoiture(daoV.findById(rs.getString(2)));
                chauffeur.setNom(rs.getString(3));
                chauffeur.setAge(rs.getInt(4));
                DaoAgence daoAg = DaoAgence.getInstance();
                chauffeur.setAgence(daoAg.getAgenceById(rs.getInt(5)));
                chauffeur.setPrix(rs.getFloat(6));
                
            }
            if(count == 0){
                return null;
            }else{
                return chauffeur;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
