/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Agence;
import entity.Location;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DataSource;

/**
 *
 * @author 3D-Artist
 */
public class DaoUser {
    
    private final Connection cnx;
    private static DaoUser instance;
    
    private DaoUser() {
        cnx = DataSource.getInstance().getConnection();
    }
    
    public static DaoUser getInstance()
    {
        if (instance == null) {
            instance = new DaoUser();
        }
        return instance; 
    }
    
    
    
    public User findUserById(int id)
    {
        User owner = new User();
        int count = 0;
           
        String requete="select * from user where id="+id;
        try{
            Statement st = cnx.createStatement();
            ResultSet rsl = st.executeQuery(requete);
            while(rsl.next())
            {
                owner.setId(rsl.getInt(1));
                owner.setNom(rsl.getString(2));
                owner.setPrenom(rsl.getString(3));
                owner.setPassword(rsl.getString(4));
                owner.setDate_naissance(rsl.getDate(5));
                owner.setTelephone(rsl.getInt(6));
                owner.setMail(rsl.getString(7));
                owner.setAdresse(rsl.getString(8));
                owner.setApprouved(rsl.getBoolean(9));
                owner.setBanned(rsl.getBoolean(10));
                owner.setRole(rsl.getString(11));
                owner.setImage(rsl.getString(12));
                count++;
            }
           if(count == 0){
                return null;
           }else{
               return owner;
           }  
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoAgence.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
   }
    
    
    public boolean checkCinExist(int id)
    {
        
        int count = 0;
           
        String requete="select * from user where id="+id;
        try{
            Statement st = cnx.createStatement();
            ResultSet rsl = st.executeQuery(requete);
            while(rsl.next())
            {
                count++;
            }
           if(count == 0){
                return false;
           }else{
               return true;
           }  
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoAgence.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   }
    
    
    public User login(String email, String pass){
        User user = new User();
        try {
            String requete = "select * from user where mail='"+email+"' and password='"+pass+"'";
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            int count = 0;
            while(rs.next()){
                count ++;
                user.setId(rs.getInt(1));
                user.setNom(rs.getString(2));
                user.setPrenom(rs.getString(3));
                user.setPassword(rs.getString(4));
                user.setDate_naissance(rs.getDate(5));
                user.setTelephone(rs.getInt(6));
                user.setMail(rs.getString(7));
                user.setAdresse(rs.getString(8));
                user.setApprouved(rs.getBoolean(9));
                user.setBanned(rs.getBoolean(10));
                user.setRole(rs.getString(11));
                user.setImage(rs.getString(12));
            }
            System.out.println(count);
            if(count == 0){
                return null;
            }else{
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoUser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
    public boolean registerClient(User client)
    {
        int numberRows;
        try {
            String requete = "insert into user set id=?,nom=?,prenom=?, password=?, date_naissance=?, telephone=?, mail=?, adresse=?, approuved=?, banned=?, Role=?, image=?";
            PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setInt(1, client.getId());
            pst.setString(2, client.getNom());
            pst.setString(3, client.getPrenom());
            pst.setString(4, client.getPassword());
            pst.setDate(5, client.getDate_naissance());
            pst.setInt(6, client.getTelephone());
            pst.setString(7, client.getMail());
            pst.setString(8, client.getAdresse());
            pst.setBoolean(9, client.isApprouved());
            pst.setBoolean(10, client.isBanned());
            pst.setString(11, client.getRole());
            pst.setString(12, client.getImage());
            numberRows = pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoChauffeur.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(numberRows == 0){
            return false;
        }else{
            return true;
        }
    }
    
    
    public boolean supprimerUser(int id)
    {
        int test=0;
        boolean check;
        try{
            List<Agence> agences = new ArrayList<>();
            agences = DaoAgence.getInstance().getAgencesByManagerID(id);
            if(agences != null){
                for(int i=0; i<agences.size();i++){
                    DaoAgence.getInstance().SupprimerAgence(agences.get(i).getId_agence());
                }
            }
            
            PreparedStatement pst = cnx.prepareStatement("delete from user where id="+id); 
            test= pst.executeUpdate();
        }
        catch (SQLException ex)
        {
            Logger.getLogger(DaoUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (test == 0)
        {
            check=false;
        }else{
            check=true;
        }
        return check ;
    }
    
    
    public boolean approuverUser(int id)
    {
        int test=0;
        boolean check;
        try{
            PreparedStatement pst = cnx.prepareStatement("update user set approuved=1 where id="+id); 
            test= pst.executeUpdate();
        }
        catch (SQLException ex)
        {
            Logger.getLogger(DaoUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (test == 0)
        {
            check=false;
        }else{
            check=true;
        }
        return check ;
    }
    
    
    public List<User> getAllUsers()
    {
        List<User> list = new ArrayList<User>();
        int count = 0;
           
        String requete="select * from user";
        try{
            Statement st = cnx.createStatement();
            ResultSet rsl = st.executeQuery(requete);
            while(rsl.next())
            {
                if(! rsl.getString(11).equals("ROLE_ADMIN")){
                    User owner = new User();
                    owner.setId(rsl.getInt(1));
                    owner.setNom(rsl.getString(2));
                    owner.setPrenom(rsl.getString(3));
                    owner.setPassword(rsl.getString(4));
                    owner.setDate_naissance(rsl.getDate(5));
                    owner.setTelephone(rsl.getInt(6));
                    owner.setMail(rsl.getString(7));
                    owner.setAdresse(rsl.getString(8));
                    owner.setApprouved(rsl.getBoolean(9));
                    owner.setBanned(rsl.getBoolean(10));
                    owner.setRole(rsl.getString(11));
                    owner.setImage(rsl.getString(12));
                    count++;
                    list.add(owner);
                }
            }
           if(count == 0){
                return null;
           }else{
               return list;
           }  
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoAgence.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
   }




}
    

