/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Agence;
import entity.Location;
import entity.User;
import entity.Voiture;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DataSource;

/**
 *
 * @author 3D-Artist
 */
public class DaoVoiture {
    
private Connection cnx;
    private static DaoVoiture instance;
    
    private DaoVoiture() {
        cnx = DataSource.getInstance().getConnection();
    }
    
    public static DaoVoiture getInstance()
        {if (instance == null) {
            instance = new DaoVoiture();
        }
        return instance;   
        }
    
    

    
    
    public boolean AjouterVoiture(Voiture v)
    {
        int nbr_ligne=0;
        try{
           String requete="insert into voiture set matricule=?,marque=?,couleur=?,carburant=?,age=?,kilometrage=?,puissance=?,disponible=?,id_user=?,carrousserie=?,boite=?,gps=?,climatisation=?,airbag=?,nbr_porte=?,frein_abs=?,alarme=?,description=?, id_agence=?, prix_location=?";
           PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setString(1,v.getMatricule());
            pst.setString(2,v.getMarque());
            pst.setString(3,v.getCouleur());
            pst.setString(4,v.getCarburant());
            pst.setDate(5, (Date) v.getAge());
            pst.setDouble(6,v.getKilometrage());
            pst.setInt(7,v.getPuissance());
            pst.setBoolean(8,v.getDisponible());
            pst.setInt(9,v.getOwner().getId());
            pst.setString(10,v.getCarrousserie());
            pst.setString(11,v.getBoite());
            pst.setBoolean(12,v.getGps());
            pst.setBoolean(13,v.getClimatisation());
            pst.setBoolean(14,v.getAirbag());
            pst.setInt(15,v.getNbr_porte());
            pst.setBoolean(16,v.getFrein_abs());
            pst.setBoolean(17,v.getAlarme());
            pst.setString(18,v.getDescription());
            pst.setInt(19,v.getAgence().getId_agence());
            pst.setFloat(20,v.getPrixLocation());
            nbr_ligne=pst.executeUpdate();
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(nbr_ligne == 0){
            return false;
        }else{
            return true;
        }
   }
   
   
    public boolean ModifierVoiture(Voiture voiture, String ancienMatricule)
    {
        int nbr_ligne;
        try{
            String requete="UPDATE voiture set matricule=?,marque=?,couleur=?,carburant=?,age=?,kilometrage=?,puissance=?,disponible=?,id_user=?,carrousserie=?,boite=?,gps=?,climatisation=?,airbag=?,nbr_porte=?,frein_abs=?,alarme=?,description=?, id_agence=?, prix_location=? where matricule=?";
            PreparedStatement pst = cnx.prepareStatement(requete);
            pst.setString(1,voiture.getMatricule());
            pst.setString(2,voiture.getMarque());
            pst.setString(3,voiture.getCouleur());
            pst.setString(4,voiture.getCarburant());
            pst.setDate(5,voiture.getAge());
            pst.setDouble(6,voiture.getKilometrage());
            pst.setInt(7,voiture.getPuissance());
            pst.setBoolean(8,voiture.isDisponible());
            pst.setInt(9,voiture.getOwner().getId());
            pst.setString(10,voiture.getCarrousserie());
            pst.setString(11,voiture.getBoite());
            pst.setBoolean(12,voiture.getGps());
            pst.setBoolean(13,voiture.getClimatisation());
            pst.setBoolean(14,voiture.getAirbag());
            pst.setInt(15,voiture.getNbr_porte());
            pst.setBoolean(16,voiture.getFrein_abs());
            pst.setBoolean(17,voiture.getAlarme());
            pst.setString(18,voiture.getDescription());
            pst.setInt(19,voiture.getAgence().getId_agence());
            pst.setFloat(20,voiture.getPrixLocation());
            pst.setString(21,ancienMatricule);
            
            nbr_ligne=pst.executeUpdate();
        }
        catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(nbr_ligne == 0){
            return false;
        }else{
            return true;
        }
    }
   
    public boolean Supprimervoiture (String id){
        int test=0;
        boolean mission;
        try{
            List<Location> locations = new ArrayList<>();
            locations = DaoLocation.getInstance().getLocationByVoiture(id);
            if(locations != null){
                for(int i=0;i<locations.size();i++){
                    DaoLocation.getInstance().supprimerLocation(locations.get(i));
                }
            }
            PreparedStatement pst = cnx.prepareStatement("delete from voiture where matricule='"+id+"'"); 
            test= pst.executeUpdate();
        }catch (SQLException ex) {
            Logger.getLogger(DaoAgence.class.getName()).log(Level.SEVERE, null, ex);
        }
        mission = test != 0;
        return mission ;
    }
   
   
   
   
   
   
   public Voiture findById(String matricule){
        Voiture voiture = new Voiture();
        int count = 0;
        try {
            String requete = "select * from voiture where matricule='"+matricule+"'";
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                count++;
                voiture.setMatricule(rs.getString(1));
                voiture.setMarque(rs.getString(2));
                voiture.setCouleur(rs.getString(3));
                voiture.setCarburant(rs.getString(4));
                voiture.setAge(rs.getDate(5));
                voiture.setKilometrage(rs.getDouble(6));
                voiture.setPuissance(rs.getInt(7));
                //voiture.set(rs.getString(8));
                voiture.setDisponible(rs.getBoolean(9));
                DaoUser daoU = DaoUser.getInstance();
                voiture.setOwner(daoU.findUserById(rs.getInt(10)));
                voiture.setCarrousserie(rs.getString(11));
                voiture.setBoite(rs.getString(12));
                voiture.setGps(rs.getBoolean(13));
                voiture.setClimatisation(rs.getBoolean(14));
                voiture.setAirbag(rs.getBoolean(15));
                voiture.setNbr_porte(rs.getInt(16));
                voiture.setFrein_abs(rs.getBoolean(17));
                voiture.setAlarme(rs.getBoolean(18));
                voiture.setDescription(rs.getString(19));
                
                DaoAgence daoAg = DaoAgence.getInstance();
                voiture.setAgence(daoAg.getAgenceById(rs.getInt(20)));
                voiture.setPrixLocation(rs.getFloat(21));
            }
            if(count == 0){
                return null;
            }else{
                return voiture;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
   
    public boolean checkMatriculeExist(String matricule){
        Voiture voiture = new Voiture();
        int count = 0;
        try {
            String requete = "select * from voiture where matricule='"+matricule+"'";
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                count++;
                voiture.setMatricule(rs.getString(1));
                voiture.setMarque(rs.getString(2));
                voiture.setCouleur(rs.getString(3));
                voiture.setCarburant(rs.getString(4));
                voiture.setAge(rs.getDate(5));
                voiture.setKilometrage(rs.getDouble(6));
                voiture.setPuissance(rs.getInt(7));
                //voiture.set(rs.getString(8));
                voiture.setDisponible(rs.getBoolean(9));
                DaoUser daoU = DaoUser.getInstance();
                voiture.setOwner(daoU.findUserById(rs.getInt(10)));
                voiture.setCarrousserie(rs.getString(11));
                voiture.setBoite(rs.getString(12));
                voiture.setGps(rs.getBoolean(13));
                voiture.setClimatisation(rs.getBoolean(14));
                voiture.setAirbag(rs.getBoolean(15));
                voiture.setNbr_porte(rs.getInt(16));
                voiture.setFrein_abs(rs.getBoolean(17));
                voiture.setAlarme(rs.getBoolean(18));
                voiture.setDescription(rs.getString(19));
                
                DaoAgence daoAg = DaoAgence.getInstance();
                voiture.setAgence(daoAg.getAgenceById(rs.getInt(20)));
                voiture.setPrixLocation(rs.getFloat(21));
            }
            if(count == 0){
                return false;
            }else{
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
   
   
    public List<Voiture> getAllByAgence(int idAgence)
    {
        List<Voiture> listVoitures = new ArrayList<>();
        try {
            String requete = "select * from voiture where id_agence="+idAgence;
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                Voiture voiture = new Voiture();
                voiture.setMatricule(rs.getString(1));
                voiture.setMarque(rs.getString(2));
                voiture.setCouleur(rs.getString(3));
                voiture.setCarburant(rs.getString(4));
                voiture.setAge(rs.getDate(5));
                voiture.setKilometrage(rs.getDouble(6));
                voiture.setPuissance(rs.getInt(7));
                //voiture.set(rs.getString(8));
                voiture.setDisponible(rs.getBoolean(9));
                DaoUser daoU = DaoUser.getInstance();
                voiture.setOwner(daoU.findUserById(rs.getInt(10)));
                voiture.setCarrousserie(rs.getString(11));
                voiture.setBoite(rs.getString(12));
                voiture.setGps(rs.getBoolean(13));
                voiture.setClimatisation(rs.getBoolean(14));
                voiture.setAirbag(rs.getBoolean(15));
                voiture.setNbr_porte(rs.getInt(16));
                voiture.setFrein_abs(rs.getBoolean(17));
                voiture.setAlarme(rs.getBoolean(18));
                voiture.setDescription(rs.getString(19));
                
                DaoAgence daoAg = DaoAgence.getInstance();
                voiture.setAgence(daoAg.getAgenceById(rs.getInt(20)));
                voiture.setPrixLocation(rs.getFloat(21));
                listVoitures.add(voiture);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listVoitures;
    }
    
    
    public List<Voiture> getAllByManager(int idManager)
    {
        List<Voiture> listVoitures = new ArrayList<>();
        try {
            String requete = "select * from voiture where id_user="+idManager;
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                Voiture voiture = new Voiture();
                voiture.setMatricule(rs.getString(1));
                voiture.setMarque(rs.getString(2));
                voiture.setCouleur(rs.getString(3));
                voiture.setCarburant(rs.getString(4));
                voiture.setAge(rs.getDate(5));
                voiture.setKilometrage(rs.getDouble(6));
                voiture.setPuissance(rs.getInt(7));
                //voiture.set(rs.getString(8));
                voiture.setDisponible(rs.getBoolean(9));
                DaoUser daoU = DaoUser.getInstance();
                voiture.setOwner(daoU.findUserById(rs.getInt(10)));
                voiture.setCarrousserie(rs.getString(11));
                voiture.setBoite(rs.getString(12));
                voiture.setGps(rs.getBoolean(13));
                voiture.setClimatisation(rs.getBoolean(14));
                voiture.setAirbag(rs.getBoolean(15));
                voiture.setNbr_porte(rs.getInt(16));
                voiture.setFrein_abs(rs.getBoolean(17));
                voiture.setAlarme(rs.getBoolean(18));
                voiture.setDescription(rs.getString(19));
                
                DaoAgence daoAg = DaoAgence.getInstance();
                voiture.setAgence(daoAg.getAgenceById(rs.getInt(20)));
                voiture.setPrixLocation(rs.getFloat(21));
                listVoitures.add(voiture);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listVoitures;
    }
    
    public List<Voiture> getAllDisponibleByAgence(int idAgence)
    {
        List<Voiture> listVoitures = new ArrayList<>();
        try {
            String requete = "select * from voiture where id_agence="+idAgence+" and disponible=true";
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                Voiture voiture = new Voiture();
                voiture.setMatricule(rs.getString(1));
                voiture.setMarque(rs.getString(2));
                voiture.setCouleur(rs.getString(3));
                voiture.setCarburant(rs.getString(4));
                voiture.setAge(rs.getDate(5));
                voiture.setKilometrage(rs.getDouble(6));
                voiture.setPuissance(rs.getInt(7));
                //voiture.set(rs.getString(8));
                voiture.setDisponible(rs.getBoolean(9));
                DaoUser daoU = DaoUser.getInstance();
                voiture.setOwner(daoU.findUserById(rs.getInt(10)));
                voiture.setCarrousserie(rs.getString(11));
                voiture.setBoite(rs.getString(12));
                voiture.setGps(rs.getBoolean(13));
                voiture.setClimatisation(rs.getBoolean(14));
                voiture.setAirbag(rs.getBoolean(15));
                voiture.setNbr_porte(rs.getInt(16));
                voiture.setFrein_abs(rs.getBoolean(17));
                voiture.setAlarme(rs.getBoolean(18));
                voiture.setDescription(rs.getString(19));
                
                DaoAgence daoAg = DaoAgence.getInstance();
                voiture.setAgence(daoAg.getAgenceById(rs.getInt(20)));
                voiture.setPrixLocation(rs.getFloat(21));
                listVoitures.add(voiture);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listVoitures;
    }
    
    public List<String> getAllMarque()
    {
        List<String> listMarque = new ArrayList<>();
        try {
            String requete = "select DISTINCT marque from voiture";
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                listMarque.add(rs.getString(1));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listMarque;
    }
    
    
    public List<Voiture> rechercheVoiture(String marque, String carburant, String carrousserie, String boite, String nbrPorte, String gps, String climatisation, String airbag, String alarme)
    {
        List<Voiture> listVoitures = new ArrayList<>();
        try {
            String requete;
            
            
            /*if(nbrPorte == 0){
                requete = "select * from voiture where marque LIKE '%"+marque+"%' and carburant LIKE '%"+carburant+"%' and carrousserie LIKE '%"+carrousserie+"%' and boite LIKE '%"+boite+"%' and gps="+gps+" and climatisation="+climatisation+" and airbag="+airbag+" and alarme="+alarme+" and disponible=true";
            }else{
                if(gps == false){
                    requete = "select * from voiture where marque LIKE '%"+marque+"%' and carburant LIKE '%"+carburant+"%' and carrousserie LIKE '%"+carrousserie+"%' and boite LIKE '%"+boite+"%' and nbr_porte="+nbrPorte+" and climatisation="+climatisation+" and airbag="+airbag+" and alarme="+alarme+" and disponible=true";
                }else{
                    if(climatisation == false){
                        requete = "select * from voiture where marque LIKE '%"+marque+"%' and carburant LIKE '%"+carburant+"%' and carrousserie LIKE '%"+carrousserie+"%' and boite LIKE '%"+boite+"%' and nbr_porte="+nbrPorte+" and gps="+gps+" and airbag="+airbag+" and alarme="+alarme+" and disponible=true";
                    }else{
                        if(airbag == false){
                            requete = "select * from voiture where marque LIKE '%"+marque+"%' and carburant LIKE '%"+carburant+"%' and carrousserie LIKE '%"+carrousserie+"%' and boite LIKE '%"+boite+"%' and nbr_porte="+nbrPorte+" and gps="+gps+" and climatisation="+climatisation+" and alarme="+alarme+" and disponible=true";
                        }else{
                            if(alarme == false){
                                requete = "select * from voiture where marque LIKE '%"+marque+"%' and carburant LIKE '%"+carburant+"%' and carrousserie LIKE '%"+carrousserie+"%' and boite LIKE '%"+boite+"%' and nbr_porte="+nbrPorte+" and gps="+gps+" and climatisation="+climatisation+" and airbag="+airbag+" and disponible=true";
                            }else{
                                requete = "select * from voiture where marque LIKE '%"+marque+"%' and carburant LIKE '%"+carburant+"%' and carrousserie LIKE '%"+carrousserie+"%' and boite LIKE '%"+boite+"%' and nbr_porte="+nbrPorte+" and gps="+gps+" and climatisation="+climatisation+" and airbag="+airbag+" and alarme="+alarme+" and disponible=true";
                            }
                        }
                    }
                }
            }*/
            
            
            
            
            
            
            requete = "select * from voiture where marque LIKE '%"+marque+"%' and carburant LIKE '%"+carburant+"%' and carrousserie LIKE '%"+carrousserie+"%' and boite LIKE '%"+boite+"%' and nbr_porte LIKE '%"+nbrPorte+"%' and gps LIKE '%"+gps+"%' and climatisation LIKE '%"+climatisation+"%' and airbag LIKE '%"+airbag+"%' and alarme LIKE '%"+alarme+"%' and disponible=true";
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                Voiture voiture = new Voiture();
                voiture.setMatricule(rs.getString(1));
                voiture.setMarque(rs.getString(2));
                voiture.setCouleur(rs.getString(3));
                voiture.setCarburant(rs.getString(4));
                voiture.setAge(rs.getDate(5));
                voiture.setKilometrage(rs.getDouble(6));
                voiture.setPuissance(rs.getInt(7));
                //voiture.set(rs.getString(8));
                voiture.setDisponible(rs.getBoolean(9));
                DaoUser daoU = DaoUser.getInstance();
                voiture.setOwner(daoU.findUserById(rs.getInt(10)));
                voiture.setCarrousserie(rs.getString(11));
                voiture.setBoite(rs.getString(12));
                voiture.setGps(rs.getBoolean(13));
                voiture.setClimatisation(rs.getBoolean(14));
                voiture.setAirbag(rs.getBoolean(15));
                voiture.setNbr_porte(rs.getInt(16));
                voiture.setFrein_abs(rs.getBoolean(17));
                voiture.setAlarme(rs.getBoolean(18));
                voiture.setDescription(rs.getString(19));
                
                DaoAgence daoAg = DaoAgence.getInstance();
                voiture.setAgence(daoAg.getAgenceById(rs.getInt(20)));
                voiture.setPrixLocation(rs.getFloat(21));
                listVoitures.add(voiture);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listVoitures;
    }
    
    
    
    public List<Voiture> getAll()
    {
        List<Voiture> listVoitures = new ArrayList<>();
        try {
            String requete = "select * from voiture";
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            
            while(rs.next()){
                Voiture voiture = new Voiture();
                voiture.setMatricule(rs.getString(1));
                voiture.setMarque(rs.getString(2));
                voiture.setCouleur(rs.getString(3));
                voiture.setCarburant(rs.getString(4));
                voiture.setAge(rs.getDate(5));
                voiture.setKilometrage(rs.getDouble(6));
                voiture.setPuissance(rs.getInt(7));
                //voiture.set(rs.getString(8));
                voiture.setDisponible(rs.getBoolean(9));
                DaoUser daoU = DaoUser.getInstance();
                voiture.setOwner(daoU.findUserById(rs.getInt(10)));
                voiture.setCarrousserie(rs.getString(11));
                voiture.setBoite(rs.getString(12));
                voiture.setGps(rs.getBoolean(13));
                voiture.setClimatisation(rs.getBoolean(14));
                voiture.setAirbag(rs.getBoolean(15));
                voiture.setNbr_porte(rs.getInt(16));
                voiture.setFrein_abs(rs.getBoolean(17));
                voiture.setAlarme(rs.getBoolean(18));
                voiture.setDescription(rs.getString(19));
                
                DaoAgence daoAg = DaoAgence.getInstance();
                voiture.setAgence(daoAg.getAgenceById(rs.getInt(20)));
                voiture.setPrixLocation(rs.getFloat(21));
                listVoitures.add(voiture);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoVoiture.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return listVoitures;
    }

}
