/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoVoiture;
import entity.Voiture;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import utils.Session;


public class FXMLEspaceClientRechercheVoitureController implements Initializable {

    @FXML
    private CheckBox check_gps;
    @FXML
    private CheckBox check_climatisation;
    @FXML
    private CheckBox check_airbag;
    @FXML
    private CheckBox check_alarme;
    
    @FXML
    private ChoiceBox choice_carburant;
    @FXML
    private ChoiceBox choice_carrouserie;
    @FXML
    private ChoiceBox choice_boite;
    @FXML
    private ChoiceBox choice_nbr_porte;
    
    @FXML
    private ChoiceBox choice_marque;
    @FXML
    private TableView table_result_search;
    @FXML
    private TableColumn column_marque;
    @FXML
    private TableColumn column_carburant;
    @FXML
    private TableColumn column_carrousserie;
    @FXML
    private TableColumn column_boite;
    @FXML
    private TableColumn column_nbr_porte;
    @FXML
    private TableColumn column_gps;
    @FXML
    private TableColumn column_climatisation;
    @FXML
    private TableColumn column_airbag;
    @FXML
    private TableColumn column_alarme;
    @FXML
    private TableColumn column_prix;
    @FXML
    private TableColumn column_agence;
    
    public static Voiture voiture = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        choice_boite.setItems(FXCollections.observableArrayList("Manuelle", "Automatique "));
        choice_nbr_porte.setItems(FXCollections.observableArrayList("2","4"));
        choice_carrouserie.setItems(FXCollections.observableArrayList("Citadine", "Compacte", "Berline","Coupé","Cabriolet","SUV","Monospace","Utilitaire","Pick Up"));
        choice_carburant.setItems(FXCollections.observableArrayList("Essence", "Diesel"));
        choice_marque.setItems(FXCollections.observableArrayList(DaoVoiture.getInstance().getAllMarque()));
        
        column_marque.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("marque")
        );
        column_carburant.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("carburant")
        );
        column_carrousserie.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("carrousserie")
        );
        column_boite.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("boite")
        );
        column_nbr_porte.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("nbr_porte")
        );
        column_gps.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("gps")
        );
        column_climatisation.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("climatisation")
        );
        column_airbag.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("airbag")
        );
        column_alarme.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("alarme")
        );
        column_prix.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("prixLocation")
        );
        column_agence.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("agence")
        );
        
        
        
        /*column_marque.setCellFactory(TextFieldTableCell.forTableColumn());
        column_marque.setOnEditCommit(
            new EventHandler<CellEditEvent<Voiture, String>>() {
                @Override
                public void handle(CellEditEvent<Voiture, String> t) {
                    ((Voiture) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setMarque(t.getNewValue());
                    System.out.println((Voiture) t.getTableView().getItems().get(
                        t.getTablePosition().getRow()));
                }
            }
        );*/
        
        
        
    }    

    @FXML
    private void chercherVoiture(ActionEvent event) {
        String marque;
        String carburant;
        String carrousserie;
        String boite;
        String nbrPorte;
        String gps;
        String climatisation;
        String airbag;
        String alarme;
        if(choice_marque.getSelectionModel().getSelectedItem() == null){
            marque = "";
        }else{
            marque = (String) choice_marque.getSelectionModel().getSelectedItem();
        }
        
        if(choice_carburant.getSelectionModel().getSelectedItem() == null){
            carburant = "";
        }else{
            carburant = (String) choice_carburant.getSelectionModel().getSelectedItem();
        }
        
        if(choice_carrouserie.getSelectionModel().getSelectedItem() == null){
            carrousserie = "";
        }else{
            carrousserie = (String) choice_carrouserie.getSelectionModel().getSelectedItem();
        }
        
        if(choice_boite.getSelectionModel().getSelectedItem() == null){
            boite = "";
        }else{
            boite = (String) choice_boite.getSelectionModel().getSelectedItem();
        }
        
        if(choice_nbr_porte.getSelectionModel().getSelectedItem() == null){
            nbrPorte = "";
        }else{
            nbrPorte = (String) choice_nbr_porte.getSelectionModel().getSelectedItem();
        }
        
        if(check_gps.isSelected()){
            gps = "1";
        }else{
            gps = "";
        }
        
        if(check_climatisation.isSelected()){
            climatisation = "1";
        }else{
            climatisation = "";
        }
        
        if(check_airbag.isSelected()){
            airbag = "1";
        }else{
            airbag = "";
        }
        
        if(check_alarme.isSelected()){
            alarme = "1";
        }else{
            alarme = "";
        }
        
        List<Voiture> listVoitures = new ArrayList<>();
        listVoitures = DaoVoiture.getInstance().rechercheVoiture(marque, carburant, carrousserie, boite, nbrPorte, gps, climatisation, airbag, alarme);
        ObservableList<Voiture> data = FXCollections.observableArrayList(listVoitures);
        
        
        
        
        table_result_search.setItems(data);
    }

    @FXML
    private void suivreReservation(ActionEvent event) {
        if(table_result_search.getSelectionModel().getSelectedItem() == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("ERROR");
                alert.setContentText("Choisir une voiture du tableau");
                alert.showAndWait();
        }else{
            voiture = (Voiture) table_result_search.getSelectionModel().getSelectedItem();
            System.out.println(voiture);
             Parent root;
             try {
                 root = FXMLLoader.load(getClass().getResource("/gui/FXMLReserverLocation.fxml"));
                 Stage myWindow = (Stage) table_result_search.getScene().getWindow();
                 Scene sc = new Scene(root);
                 myWindow.setScene(sc);
                 myWindow.setTitle("page name");
                 myWindow.show();
             } catch (IOException ex) {
                 Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    }

    @FXML
    private void logOut(MouseEvent event) {
        Session.setLoggedInUser(null);
        Parent root;
             try {
                 root = FXMLLoader.load(getClass().getResource("/gui/FXMLLogin.fxml"));
                 Stage myWindow = (Stage) table_result_search.getScene().getWindow();
                 Scene sc = new Scene(root);
                 myWindow.setScene(sc);
                 myWindow.setTitle("Login");
                 myWindow.show();
             } catch (IOException ex) {
                 Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
             }
    }

    @FXML
    private void toutVoitures(ActionEvent event) {
        List<Voiture> listVoitures = new ArrayList<>();
        listVoitures = DaoVoiture.getInstance().getAll();
        ObservableList<Voiture> data = FXCollections.observableArrayList(listVoitures);
        
        table_result_search.setItems(data);
    }
    
    
    
}
