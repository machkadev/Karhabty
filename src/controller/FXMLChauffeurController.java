/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoAgence;
import dao.DaoChauffeur;
import dao.DaoUser;
import dao.DaoVoiture;
import entity.Agence;
import entity.Chauffeur;
import entity.User;
import entity.Voiture;
import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.converter.FloatStringConverter;
import javafx.util.converter.IntegerStringConverter;
import utils.Session;


public class FXMLChauffeurController implements Initializable {
      //Les textField
    @FXML
    private TextField input_NomChauffeur;
    @FXML
    private TextField input_AgeChauffeur;
    @FXML
    private TextField input_PrixChauffeur;
    @FXML
    private ChoiceBox input_AgenceChauffeur;
    
    /*Les Boutons*/
    @FXML
    private Button button_AjouterChauffeur;
    
    @FXML
    private Button button_SupprimerChauffeur;
    
    
    
    @FXML Label label_error;
    
    
    
    @FXML
    private TableColumn column_identifiantChauffeur;
    @FXML
    private TableView List_chauffeur;
    @FXML
    private TableColumn column_NomChauffeur;
    @FXML
    private TableColumn column_AgeChauffeur;
    @FXML
    private TableColumn column_AgenceChauffeur;
    @FXML
    private TableColumn column_PrixChauffeur;
    
 
    

    @FXML
    private void ajouterchauffeur(ActionEvent event) {
    
    
        
                    if(input_NomChauffeur.getText().equals("") || input_AgeChauffeur.getText().equals("") || input_PrixChauffeur.getText().equals("") ||input_AgenceChauffeur.getSelectionModel().getSelectedItem() == null ){
                        label_error.setText("Remlir toute les champs");
                     } else{
                        
                        label_error.setText("");
                        
                        float prix = Float.parseFloat(input_PrixChauffeur.getText());
                        int age=Integer.parseInt(input_AgeChauffeur.getText());


                        DaoChauffeur daochauffeur = DaoChauffeur.getInstance();
                        Agence agence = DaoAgence.getInstance().getAgenceByName((String) input_AgenceChauffeur.getSelectionModel().getSelectedItem());
                        //Chauffeur ch= DaoChauffeur.getInstance().findById(1);
                        System.out.println("gggggggggggggggg");
                         System.out.println(agence.getId_agence());

                         Voiture v = new Voiture();
                         v.setMatricule(null);
                        Chauffeur chauffeur= new Chauffeur(v,input_NomChauffeur.getText(),age,agence,prix );

                        if(daochauffeur.ajouterChauffeur(chauffeur)){
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("SUCCESS");
                            alert.setHeaderText("SUCCESS");
                            alert.setContentText("Ajout avec succée!");
                            alert.showAndWait();
                            resetTableData();
                        }else{
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText("ERROR");
                            alert.setContentText("Erreur lors de l'ajout!");
                            alert.showAndWait();
                        }
                    
                    
                    }

           
                
    }

                

   
    
    
    
    
    public void resetTableData()
    {
        List<Chauffeur> Table_Chauffeur = new ArrayList<>();
        DaoChauffeur daoCh = DaoChauffeur.getInstance();
        Table_Chauffeur = daoCh.getAllByManager(Session.getLoggedInUser().getId());//TODO : SET USER FROM SESSION
        ObservableList<Chauffeur> data = FXCollections.observableArrayList(Table_Chauffeur);
        List_chauffeur.setItems(data);        
    }




 @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        
        DaoAgence daoAg = DaoAgence.getInstance();
        List<Agence> listeAgences = daoAg.getAgencesByManagerID(Session.getLoggedInUser().getId());//TODO : SET USER FROM SESSION
        List<String> listeNomAgences = new ArrayList<>();
        for(int i=0; i < listeAgences.size(); i++){
            listeNomAgences.add(listeAgences.get(i).getNom_agence());
        }
        input_AgenceChauffeur.setItems(FXCollections.observableArrayList(listeNomAgences));
    
        
        
        //FILTER TEXTFIELD TO ENTER ONLY NUMBERS
        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("-?([1-9][0-9]*)?")) { 
                return change;
            }
            return null;
        };
        input_AgeChauffeur.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
        input_PrixChauffeur.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
        
        
        
        
        
        List<Chauffeur> listChauffeur = new ArrayList<>();
        DaoChauffeur daoCh = DaoChauffeur.getInstance();
        listChauffeur = daoCh.getAllByManager(Session.getLoggedInUser().getId());//TODO : SET USER FROM SESSION
        System.out.println(listChauffeur);
        
        ObservableList<Chauffeur> data = FXCollections.observableArrayList(listChauffeur);
        column_identifiantChauffeur.setCellValueFactory(
            new PropertyValueFactory<Chauffeur,String>("id")
        );
        column_NomChauffeur.setCellValueFactory(
            new PropertyValueFactory<Chauffeur,String>("nom")
        );
        column_AgeChauffeur.setCellValueFactory(
            new PropertyValueFactory<Chauffeur,String>("age")
        );
        column_AgenceChauffeur.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("agence")
        );
        column_PrixChauffeur.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("prix")
        );
        
        
        List_chauffeur.setItems(data);
        
        
          column_NomChauffeur.setCellFactory(TextFieldTableCell.forTableColumn());
            column_NomChauffeur.setOnEditCommit(
                new EventHandler<CellEditEvent<Chauffeur, String>>() {
                @Override
                public void handle(CellEditEvent<Chauffeur, String> t) {
                    ((Chauffeur) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setNom(t.getNewValue());
                    Chauffeur c = (Chauffeur) t.getTableView().getItems().get(t.getTablePosition().getRow());
                    c.setNom(t.getNewValue());
                    Voiture v = new Voiture();
                    v.setMatricule(null);
                    c.setVoiture(v);
                    DaoChauffeur.getInstance().modifierChauffeur(c);
                    
                }
            }
        ); 
            
            
            
             
            
            
        
    }    

    @FXML
    private void supprimerChauffeur(ActionEvent event) {
        Chauffeur c = (Chauffeur) List_chauffeur.getSelectionModel().getSelectedItem();
        if(c == null){
             Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error");
                    alert.setContentText("choisir un chauffeur du tableau");
                    alert.showAndWait();
                    resetTableData();
        }else{
            DaoChauffeur.getInstance().supprimerChauffeur(c.getId());
             Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("SUCCESS");
                    alert.setHeaderText("SUCCESS");
                    alert.setContentText("supprimer avec succée!");
                    alert.showAndWait();
                    resetTableData();
            resetTableData();
        }
    }
    
    
    @FXML
    private void logOut() {
        Session.setLoggedInUser(null);
        Parent root;
             try {
                 root = FXMLLoader.load(getClass().getResource("/gui/FXMLLogin.fxml"));
                 Stage myWindow = (Stage) List_chauffeur.getScene().getWindow();
                 Scene sc = new Scene(root);
                 myWindow.setScene(sc);
                 myWindow.setTitle("Login");
                 myWindow.show();
             } catch (IOException ex) {
                 Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
             }
    }

    
    
    
}