/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.InfoWindow;
import com.lynden.gmapsfx.javascript.object.InfoWindowOptions;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author hattab_company
 */
public class FXMLMapController implements Initializable, MapComponentInitializedListener {

    @FXML
    private GoogleMapView mapView;
    
    private GoogleMap map;
    
    public static double latAgence = 0;
    public static double lngAgence = 0;
    public static String agenceName = "";

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        mapView.addMapInializedListener(this);
    }    

    @Override
    public void mapInitialized() {
        
        
        LatLong agencePosition = new LatLong(latAgence, lngAgence);
        
        
        //Set the initial properties of the map.
        MapOptions mapOptions = new MapOptions();
        
        mapOptions.center(agencePosition)
//                .mapType(MapType.ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(12);
                   
        map = mapView.createMap(mapOptions);

        //Add markers to the map
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(agencePosition).visible(Boolean.TRUE).title(agenceName);
        
        
        
       
        Marker agenceMarker = new Marker(markerOptions);
        
        
       
        map.addMarker( agenceMarker );
        
        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content(agenceName);
                                

        InfoWindow organismeInfoWindow = new InfoWindow(infoWindowOptions);
        organismeInfoWindow.open(map, agenceMarker);
    }

    
    @FXML
    private void zoomOut() {
        map.setZoom(map.getZoom()-2);
    }

   
    @FXML
    private void zoomIn() {
        map.setZoom(map.getZoom()+2);
    }
    
}
