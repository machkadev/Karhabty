/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;


public class FXMLEspaceManagerLocationController implements Initializable {

    @FXML
    private AnchorPane pane_chauffeur;
    @FXML
    private AnchorPane pane_voitures;
    @FXML
    private AnchorPane pane_ListeDemande;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/FXMLChauffeur.fxml"));
            pane_chauffeur.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAjouterAgenceLocation1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/gui/FXMLAjoutListeVoiture.fxml"));
            pane_voitures.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAjouterAgenceLocation1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            AnchorPane pane;
            pane = FXMLLoader.load(getClass().getResource("/gui/FXMLListeDesDemandes.fxml"));
            pane_ListeDemande.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAjouterAgenceLocation1Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
}
