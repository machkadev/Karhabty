/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoAgence;
import dao.DaoUser;
import entity.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import utils.MaterialDesignButton;
import utils.Pages;
import utils.Session;


public class FXMLLoginController implements Initializable {

    @FXML
    private TextField input_email;
    @FXML
    private PasswordField input_password;
    @FXML
    private Label label_error;
    
   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        System.out.println(Session.getLoggedInUser());
        
        
    }    

    @FXML
    private void login(ActionEvent event) throws IOException {
        /*Stage mapStage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/gui/FXMLMap.fxml"));
        Scene sc = new Scene(root);
        mapStage.setScene(sc);
        mapStage.setTitle("Map");
        mapStage.show();*/
        
        
        
        
        if(input_email.getText().equals("") || input_password.getText().equals("")){
            label_error.setText("fill in the blanks");
            return;
        }
        label_error.setText("");
        DaoUser daoU = DaoUser.getInstance();
        User result = daoU.login(input_email.getText(), input_password.getText());
        if(result == null){
            label_error.setText("Bad credentials");
        }else{
            if(result.isApprouved()){
                if(result.isBanned()){
                    label_error.setText("You are banned");
                }else{
                    
                    
                    if(result.getRole().equals("ROLE_MANAGER_LOCATION")){
                        result.setListeAgences(DaoAgence.getInstance().getAgencesByManagerID(result.getId()));
                        Session.setLoggedInUser(result);
                        // TODO: Proceed to other page
                        Parent root;
                        try {
                            root = FXMLLoader.load(getClass().getResource("/gui/FXMLEspaceManagerLocation.fxml"));
                            Stage myWindow = (Stage) input_email.getScene().getWindow();
                            Scene sc = new Scene(root);
                            myWindow.setScene(sc);
                            myWindow.setTitle("page name");
                            //myWindow.setFullScreen(true);
                            myWindow.show();
                        } catch (IOException ex) {
                            Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if(result.getRole().equals("ROLE_CLIENT")){
                        Session.setLoggedInUser(result);
                        // TODO: Proceed to other page
                        Parent root;
                        try {
                            root = FXMLLoader.load(getClass().getResource("/gui/FXMLEspaceClientRechercheVoiture.fxml"));
                            Stage myWindow = (Stage) input_email.getScene().getWindow();
                            Scene sc = new Scene(root);
                            myWindow.setScene(sc);
                            myWindow.setTitle("page name");
                            //myWindow.setFullScreen(true);
                            myWindow.show();
                        } catch (IOException ex) {
                            Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else if(result.getRole().equals("ROLE_ADMIN")){
                        Session.setLoggedInUser(result);
                        // TODO: Proceed to other page
                        Parent root;
                        try {
                            root = FXMLLoader.load(getClass().getResource("/gui/FXMLEspaceAdmin.fxml"));
                            Stage myWindow = (Stage) input_email.getScene().getWindow();
                            Scene sc = new Scene(root);
                            myWindow.setScene(sc);
                            myWindow.setTitle("Admin");
                            //myWindow.setFullScreen(true);
                            myWindow.show();
                        } catch (IOException ex) {
                            Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }else{
                label_error.setText("Tu n'es pas approuve encore");
            }
            
            System.out.println(Session.getLoggedInUser());
        }
    }

    @FXML
    private void goToRegister(ActionEvent event) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/gui/FXMLCHOIXROLESIGNUP.fxml"));
            Stage myWindow = (Stage) input_email.getScene().getWindow();
            Scene sc = new Scene(root);
            myWindow.setScene(sc);
            myWindow.setTitle(Pages.REGISTER_CHOOSE_TYPE_USER);
            //myWindow.setFullScreen(true);
            myWindow.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

  
    
}
