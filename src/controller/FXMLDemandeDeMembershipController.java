/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoAgence;
import dao.DaoUser;
import dao.DaoVoiture;
import entity.Agence;
import entity.User;
import entity.Voiture;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;


public class FXMLDemandeDeMembershipController implements Initializable {

    @FXML
    private TableView table_agences;
    @FXML
    private TableColumn column_nom_agence;
    @FXML
    private TableColumn column_manager;
    @FXML
    private TableColumn column_approuve_manager;
    @FXML
    private TableColumn column_type_agence;
    @FXML
    private TableColumn column_ville;
    @FXML
    private TableColumn column_piece_justificatif;

    /**
     * Initializes the controller class.
     */
    
    public void resetTable()
    {
        List<Agence> listAgences = new ArrayList<>();
        
        listAgences = DaoAgence.getInstance().getAllAgencesNonApprouved();
        ObservableList<Agence> data = FXCollections.observableArrayList(listAgences);
        table_agences.setItems(data);
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        column_nom_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("nom_agence")
        );
        column_manager.setCellValueFactory(new Callback<CellDataFeatures<Agence, String>, ObservableValue<String>>() {  
            @Override
            public ObservableValue<String> call(CellDataFeatures<Agence, String> param) {
                return new SimpleStringProperty(param.getValue().getOwner().getNom() + " " + param.getValue().getOwner().getPrenom());
            }
        });
        column_approuve_manager.setCellValueFactory(new Callback<CellDataFeatures<Agence, String>, ObservableValue<String>>() {  
            @Override
            public ObservableValue<String> call(CellDataFeatures<Agence, String> param) {
                if(param.getValue().getOwner().isApprouved()){
                    return new SimpleStringProperty("approuved");
                }else{
                    return new SimpleStringProperty("still");
                }
            }
        });
        column_type_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("type_agence")
        );
        column_ville.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("ville")
        );
        column_piece_justificatif.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("piece_justificatif")
        );
        resetTable();
    }    

    @FXML
    private void supprimer(ActionEvent event) {
        Agence agenceSelected = (Agence) table_agences.getSelectionModel().getSelectedItem();
        if(agenceSelected == null){
            
        }else{
            if(DaoAgence.getInstance().SupprimerAgence(agenceSelected.getId_agence())){
                DaoUser.getInstance().approuverUser(agenceSelected.getOwner().getId());
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Succée");
                alert.setContentText("agence supprimer");
                alert.showAndWait();
                resetTable();
            }else{
                
            }
        }
    }

    @FXML
    private void approuver(ActionEvent event) {
        Agence agenceSelected = (Agence) table_agences.getSelectionModel().getSelectedItem();
        if(agenceSelected == null){
            
        }else{
            if(DaoAgence.getInstance().approuverAgence(agenceSelected.getId_agence())){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Succée");
                alert.setContentText("agence approuver");
                alert.showAndWait();
                DaoUser.getInstance().approuverUser(agenceSelected.getOwner().getId());
                resetTable();
            }else{
                
            }
        }
    }

    @FXML
    private void refresh(ActionEvent event) {
        resetTable();
    }
    
}
