/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoAgence;
import dao.DaoUser;
import entity.Agence;
import entity.User;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class FXMLListeMembreKarhabtyController implements Initializable {

    @FXML
    private TableView table_users;
    @FXML
    private TableColumn column_nom;
    @FXML
    private TableColumn column_prenom;
    @FXML
    private TableColumn column_date;
    @FXML
    private TableColumn column_tel;
    @FXML
    private TableColumn column_mail;
    @FXML
    private TableColumn column_role;
    @FXML
    private TableColumn column_adr;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        column_nom.setCellValueFactory(
            new PropertyValueFactory<User,String>("nom")
        );
        column_prenom.setCellValueFactory(
            new PropertyValueFactory<User,String>("prenom")
        );
        column_date.setCellValueFactory(
            new PropertyValueFactory<User,String>("date_naissance")
        );
        column_tel.setCellValueFactory(
            new PropertyValueFactory<User,String>("telephone")
        );
        column_mail.setCellValueFactory(
            new PropertyValueFactory<User,String>("mail")
        );
        column_role.setCellValueFactory(
            new PropertyValueFactory<User,String>("role")
        );
        column_adr.setCellValueFactory(
            new PropertyValueFactory<User,String>("adresse")
        );
        resetTable();
    }    
    
    public void resetTable()
    {
        List<User> listUsers = new ArrayList<>();
        
        listUsers = DaoUser.getInstance().getAllUsers();
        if(listUsers != null){
            ObservableList<User> data = FXCollections.observableArrayList(listUsers);
            System.out.println("useeeeeeeeeeeeeeeeeeeeeeee");
            System.out.println(listUsers);
            table_users.setItems(data);
        }
        
    }

    @FXML
    private void supprimer(ActionEvent event) {
        User userSelected = (User) table_users.getSelectionModel().getSelectedItem();
        if(userSelected == null){
            
        }else{
            if(DaoUser.getInstance().supprimerUser(userSelected.getId())){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Succée");
                alert.setContentText("user supprimé");
                alert.showAndWait();
                resetTable();
            }else{
                
            }
        }
    }

    @FXML
    private void refresh(ActionEvent event) {
        resetTable();
    }
    
}
