/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoAgence;
import entity.Agence;
import entity.User;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author 3D-Artist
 */
public class FXMLListeAgenceLocationController implements Initializable {
    
    @FXML TableView table_liste_agences;
    @FXML TableColumn column_nom_agence;
    @FXML TableColumn column_telephone_agence;
    @FXML TableColumn column_horaire_agence;
    @FXML TableColumn column_manager_agence;
    @FXML TableColumn column_rue_agence;
    @FXML TableColumn column_code_postal_agence;
    @FXML TableColumn column_ville_agence;
    
    @FXML TextField input_search;
    
    
    public void resetTableData()
    {
        List<Agence> listAgences = new ArrayList<>();
        DaoAgence daoAg = DaoAgence.getInstance();
        listAgences = daoAg.getAllAgences();
        ObservableList<Agence> data = FXCollections.observableArrayList(listAgences);
        table_liste_agences.setItems(data);
    }
    
    @FXML
    private void search(KeyEvent event) {
        List<Agence> listAgences = new ArrayList<>();
        DaoAgence daoAg = DaoAgence.getInstance();
        listAgences = daoAg.searchAgenceByNameByVilleByRue(input_search.getText(),input_search.getText(),input_search.getText());
        ObservableList<Agence> data = FXCollections.observableArrayList(listAgences);
        table_liste_agences.setItems(data);
    }
    
    @FXML
    public void showAction(MouseEvent ev){
        if(ev.getButton().equals(MouseButton.PRIMARY)){
            if(ev.getClickCount() == 2){
                System.out.println("Double clicked");
            }
        }
        Agence ag = (Agence) table_liste_agences.getSelectionModel().getSelectedItem();
        System.out.println(ag);
    }
    
   
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        List<Agence> listAgences = new ArrayList<>();
        DaoAgence daoAg = DaoAgence.getInstance();
        listAgences = daoAg.getAllAgences();
        
        ObservableList<Agence> data = FXCollections.observableArrayList(listAgences);
        
        column_nom_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("nom_agence")
        );
        column_telephone_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("telephone_agence")
        );
        column_horaire_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("horaire_travail")
        );
        column_manager_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("owner")
        );
        column_rue_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("rue")
        );
        column_code_postal_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("code_postal")
        );
        column_ville_agence.setCellValueFactory(
            new PropertyValueFactory<Agence,String>("ville")
        );
        
        table_liste_agences.setItems(data);
    }    

    @FXML
    private void supprimer(ActionEvent event) {
        Agence agenceSelected = (Agence) table_liste_agences.getSelectionModel().getSelectedItem();
        if(agenceSelected == null){
            
        }else{
            if(DaoAgence.getInstance().SupprimerAgence(agenceSelected.getId_agence())){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Succée");
                alert.setContentText("agence supprimé");
                alert.showAndWait();
                resetTableData();
            }else{
                
            }
        }
    }

    @FXML
    private void voirMap(ActionEvent event) throws IOException {
        Agence agMap = (Agence) table_liste_agences.getSelectionModel().getSelectedItem();
        if(agMap == null){
            
        }else{
            FXMLMapController.agenceName = agMap.getNom_agence();
            FXMLMapController.latAgence = agMap.getLatitude();
            FXMLMapController.lngAgence = agMap.getLongitude();
            
            Stage mapStage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("/gui/FXMLMap.fxml"));
            Scene sc = new Scene(root);
            mapStage.setScene(sc);
            mapStage.setTitle("Map");
            mapStage.show();
        }
    }

    @FXML
    private void refresh(ActionEvent event) {
        resetTableData();
    }

    
}
