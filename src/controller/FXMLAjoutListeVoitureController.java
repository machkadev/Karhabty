/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DaoAgence;
import dao.DaoChauffeur;
import dao.DaoUser;
import dao.DaoVoiture;
import entity.Agence;
import entity.Chauffeur;
import entity.User;
import entity.Voiture;
import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import static javafx.scene.input.KeyCode.S;
import static javafx.scene.input.KeyCode.T;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;
import utils.Session;

/**
 * FXML Controller class
 *
 * @author 3D-Artist
 */
public class FXMLAjoutListeVoitureController implements Initializable {
    
    
    @FXML TextField input_matricule;
    TextField input_marque;
    @FXML TextField input_couleur;
    TextField input_carburant;
    @FXML TextField input_kilometrage;
    @FXML TextField input_puissance;
    @FXML DatePicker picker_age;
    @FXML ChoiceBox choice_boite;
    @FXML TextArea input_description;
    @FXML CheckBox check_gps;
    @FXML CheckBox check_climatisation;
    @FXML CheckBox check_airbag;
    @FXML CheckBox check_frein_abs;
    @FXML CheckBox check_alarme;
    @FXML Label label_error;
    @FXML TextField input_prix;
    @FXML ChoiceBox choice_agence;
    @FXML ChoiceBox choice_nbr_porte;
    @FXML ChoiceBox choice_carrouserie;
    @FXML TableView table_list_voitures;
    @FXML
    private TableColumn column_matricule;
    @FXML
    private TableColumn comumn_marque;
    @FXML
    private TableColumn comumn_couleur;
    @FXML
    private TableColumn comumn_carburant;
    @FXML
    private TableColumn comumn_age;
    @FXML
    private TableColumn comumn_kilometrage;
    @FXML
    private TableColumn comumn_puissance;
    @FXML
    private TableColumn comumn_carrousserie;
    @FXML
    private TableColumn comumn_boite;
    @FXML
    private TableColumn comumn_gps;
    @FXML
    private TableColumn comumn_climatisation;
    @FXML
    private TableColumn comumn_airbag;
    @FXML
    private TableColumn comumn_nbr_porte;
    @FXML
    private TableColumn comumn_frein_abs;
    @FXML
    private TableColumn comumn_alarme;
    @FXML
    private TableColumn column_agence;
    @FXML
    private ChoiceBox choice_carburant;
    @FXML
    private ChoiceBox choice_marque;
    
    
    
    
    @FXML
    private void ajouterVoiture(ActionEvent event) {
        System.out.println(choice_boite.getSelectionModel().getSelectedItem());
        if(input_matricule.getText().equals("") || choice_marque.getSelectionModel().getSelectedItem() == null || input_couleur.getText().equals("") || choice_carburant.getSelectionModel().getSelectedItem() == null || input_kilometrage.getText().equals("") || input_puissance.getText().equals("") || choice_nbr_porte.getSelectionModel().getSelectedItem() == null || choice_boite.getSelectionModel().getSelectedItem() == null || picker_age.getValue() == null || choice_carrouserie.getSelectionModel().getSelectedItem() == null || input_prix.getText().equals("") || choice_agence.getSelectionModel().getSelectedItem() == null){
            label_error.setText("Fill in the blanks");
        }else{
            label_error.setText("");
            
            if(DaoVoiture.getInstance().checkMatriculeExist(input_matricule.getText())){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("ERROR");
                alert.setContentText("Cette Matricule existe deja !");
                alert.showAndWait();
            }else{
                java.util.Date date = java.util.Date.from(picker_age.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());

                float prix = Float.parseFloat(input_prix.getText());
                double kilometrage = Double.parseDouble(input_kilometrage.getText());
                int puissance = Integer.parseInt(input_puissance.getText());

                DaoVoiture daoV = DaoVoiture.getInstance();
                Agence agence = DaoAgence.getInstance().getAgenceByName((String) choice_agence.getSelectionModel().getSelectedItem());
                User owner = DaoUser.getInstance().findUserById(Session.getLoggedInUser().getId());//TODO : SET USER FROM SESSION
                int nbrPorte = Integer.parseInt((String)choice_nbr_porte.getSelectionModel().getSelectedItem());
                Voiture v = new Voiture(input_matricule.getText(), (String) choice_marque.getSelectionModel().getSelectedItem(), input_couleur.getText(), (String) choice_carburant.getSelectionModel().getSelectedItem(), sqlDate, kilometrage, puissance, true, owner, (String)choice_carrouserie.getSelectionModel().getSelectedItem(), (String) choice_boite.getSelectionModel().getSelectedItem(), check_gps.isSelected(), check_climatisation.isSelected(), check_airbag.isSelected(), nbrPorte, check_frein_abs.isSelected(), check_alarme.isSelected(), input_description.getText(), agence, prix);
                if(daoV.AjouterVoiture(v)){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("SUCCESS");
                    alert.setHeaderText("SUCCESS");
                    alert.setContentText("Ajout avec succée!");
                    alert.showAndWait();
                    resetTableData();
                }else{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("ERROR");
                    alert.setHeaderText("ERROR");
                    alert.setContentText("Erreur lors de l'ajout!");
                    alert.showAndWait();
                }
            }
        }
    }
    
    
    public void resetTableData()
    {
        List<Voiture> listVoitures = new ArrayList<>();
        DaoVoiture daoV = DaoVoiture.getInstance();
        listVoitures = daoV.getAllByManager(Session.getLoggedInUser().getId());//TODO : SET USER FROM SESSION
        ObservableList<Voiture> data = FXCollections.observableArrayList(listVoitures);
        table_list_voitures.setItems(data);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        choice_boite.setItems(FXCollections.observableArrayList("Manuelle", "Automatique "));
        choice_nbr_porte.setItems(FXCollections.observableArrayList("2","4"));
        choice_carrouserie.setItems(FXCollections.observableArrayList("Citadine", "Compacte", "Berline","Coupé","Cabriolet","SUV","Monospace","Utilitaire","Pick Up"));
        choice_carburant.setItems(FXCollections.observableArrayList("Essence", "Diesel"));
        choice_marque.setItems(FXCollections.observableArrayList("BMW", "Peugeot","Citroen","Ford","Mercedes","Fiat","Mazda","Jeep","Jaguar","Porsh","Toyota","Renault"));
        
        
        DaoAgence daoAg = DaoAgence.getInstance();
        List<Agence> listeAgences = daoAg.getAgencesByManagerID(Session.getLoggedInUser().getId());//TODO : SET USER FROM SESSION
        List<String> listeNomAgences = new ArrayList<>();
        for(int i=0; i < listeAgences.size(); i++){
            listeNomAgences.add(listeAgences.get(i).getNom_agence());
        }
        choice_agence.setItems(FXCollections.observableArrayList(listeNomAgences));
        
        
        
        //FILTER TEXTFIELD TO ENTER ONLY NUMBERS
        UnaryOperator<Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("-?([1-9][0-9]*)?")) { 
                return change;
            }
            return null;
        };
        input_puissance.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
        input_kilometrage.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
        
        
        
        
        
        List<Voiture> listVoitures = new ArrayList<>();
        DaoVoiture daoV = DaoVoiture.getInstance();
        listVoitures = daoV.getAllByManager(Session.getLoggedInUser().getId());//TODO : SET USER FROM SESSION
        
        ObservableList<Voiture> data = FXCollections.observableArrayList(listVoitures);
        column_matricule.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("matricule")
        );
        comumn_marque.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("marque")
        );
        comumn_couleur.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("couleur")
        );
        comumn_carburant.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("carburant")
        );
        comumn_age.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("age")
        );
        comumn_kilometrage.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("kilometrage")
        );
        comumn_puissance.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("puissance")
        );
        comumn_carrousserie.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("carrousserie")
        );
        comumn_boite.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("boite")
        );comumn_gps.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("gps")
        );
        comumn_climatisation.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("climatisation")
        );
        comumn_airbag.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("airbag")
        );
        comumn_nbr_porte.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("nbr_porte")
        );
        comumn_frein_abs.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("frein_abs")
        );
        comumn_alarme.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("alarme")
        );
        column_agence.setCellValueFactory(
            new PropertyValueFactory<Voiture,String>("agence")
        );
        
        table_list_voitures.setItems(data);
        
        /*************Modification*************/
        //modifier couleur
                
        comumn_couleur.setCellFactory(TextFieldTableCell.forTableColumn());
           comumn_couleur.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Voiture, String>>() {
                    
                 @Override
                public void handle(TableColumn.CellEditEvent<Voiture, String> t) {
                    ((Voiture) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setCouleur(t.getNewValue());
                   Voiture v = (Voiture) t.getTableView().getItems().get(t.getTablePosition().getRow());
                   v.setCouleur(t.getNewValue());
                   
                    DaoVoiture.getInstance().ModifierVoiture(v,v.getMatricule());
                };
            
           
 
                   });
           
         //modifier carburant
            comumn_carburant.setCellFactory(TextFieldTableCell.forTableColumn());
           comumn_carburant.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Voiture, String>>() {
                    
                 @Override
                public void handle(TableColumn.CellEditEvent<Voiture, String> t) {
                    ((Voiture) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setCarburant(t.getNewValue());
                    Voiture v = (Voiture) t.getTableView().getItems().get(t.getTablePosition().getRow());
                    v.setCarburant(t.getNewValue());
                    
                   
                    DaoVoiture.getInstance().ModifierVoiture(v,v.getMatricule());
                };
            
           
 
                   });
           //modifier Carrousserie
            comumn_carrousserie.setCellFactory(TextFieldTableCell.forTableColumn());
           comumn_carrousserie.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Voiture, String>>() {
                    
                 @Override
                public void handle(TableColumn.CellEditEvent<Voiture, String> t) {
                    ((Voiture) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setCarrousserie(t.getNewValue());
                    Voiture v = (Voiture) t.getTableView().getItems().get(t.getTablePosition().getRow());
                    v.setCarrousserie(t.getNewValue());
                   
                    DaoVoiture.getInstance().ModifierVoiture(v,v.getMatricule());
                };
            
           
 
                   });
           //modifier Boite
            comumn_boite.setCellFactory(TextFieldTableCell.forTableColumn());
           comumn_boite.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Voiture, String>>() {
                    
                 @Override
                public void handle(TableColumn.CellEditEvent<Voiture, String> t) {
                    ((Voiture) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setBoite(t.getNewValue());
                    Voiture v = (Voiture) t.getTableView().getItems().get(t.getTablePosition().getRow());
                    v.setBoite(t.getNewValue());
                   
                    DaoVoiture.getInstance().ModifierVoiture(v,v.getMatricule());
                };
            
           
 
                   });
    }
    
    
    @FXML
    private void logOut() {
        Session.setLoggedInUser(null);
        Parent root;
             try {
                 root = FXMLLoader.load(getClass().getResource("/gui/FXMLLogin.fxml"));
                 Stage myWindow = (Stage) table_list_voitures.getScene().getWindow();
                 Scene sc = new Scene(root);
                 myWindow.setScene(sc);
                 myWindow.setTitle("Login");
                 myWindow.show();
             } catch (IOException ex) {
                 Logger.getLogger(FXMLLoginController.class.getName()).log(Level.SEVERE, null, ex);
             }
    }

    @FXML
    private void supprimerVoiture(ActionEvent event)
    {Voiture v = (Voiture) table_list_voitures.getSelectionModel().getSelectedItem();
        if(v == null){
             Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error");
                    alert.setContentText("choisir une voiture du tableau");
                    alert.showAndWait();
                    resetTableData();
        }else{
            DaoVoiture.getInstance().Supprimervoiture(v.getMatricule());
             Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("SUCCESS");
                    alert.setHeaderText("SUCCESS");
                    alert.setContentText("supprimer avec succée!");
                    alert.showAndWait();
                    resetTableData();
            resetTableData();
        }
    }
    
}

    
    
    
                
